package problem.statement.dto;


import java.util.Collection;

public class OutputJson {
    private String collectionId;
    private Collection<Site> sites;

    public OutputJson(String collectionId, Collection<Site> sites) {
        this.collectionId = collectionId;
        this.sites = sites;
    }

    public OutputJson() {
    }

    public String getCollectionId() {
        return collectionId;
    }

    public Collection<Site> getSites() {
        return sites;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public void setSites(Collection<Site> sites) {
        this.sites = sites;
    }
}
