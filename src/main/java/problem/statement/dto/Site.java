package problem.statement.dto;


public class Site {
    private String id;
    private String name;
    private byte mobile;
    private String keywords;
    private int score;

    public Site() {
    }

    public Site(String id, String name, byte mobile, String keywords, int score) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.keywords = keywords;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte getMobile() {
        return mobile;
    }

    public String getKeywords() {
        return keywords;
    }

    public int getScore() {
        return score;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(byte mobile) {
        this.mobile = mobile;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
