package problem.statement;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import problem.statement.util.FileUtil;

public class EtlManager {
    private static Logger logger = LogManager.getLogger(EtlManager.class);
    private final InputDataProcessor csvProcessor;
    private final InputDataProcessor jsonProcessor;

    public EtlManager(InputDataProcessor csvProcessor, InputDataProcessor jsonProcessor) {
        this.csvProcessor = csvProcessor;
        this.jsonProcessor = jsonProcessor;
    }

    public void processInputData(String pathToDirectory, String outputFile) throws IOException {

        if (FileUtil.ensureDirectoryExists(pathToDirectory)) {
            logger.info("Fetch input files from {} directory", pathToDirectory);
            Path csvFilePath = Paths.get(pathToDirectory, "input1.csv");
            Path jsonFilePath = Paths.get(pathToDirectory, "input2.json");
            String jsonFromCsvFile = csvProcessor.processFile(getFileName(csvFilePath));
            String jsonFromJsonFile = jsonProcessor.processFile(getFileName(jsonFilePath));

            try (PrintWriter out = new PrintWriter(outputFile)) {
                out.println(jsonFromCsvFile);
                out.println(jsonFromJsonFile);
            }

        } else {
            logger.error("Unable to access {} input directory", pathToDirectory);
        }

    }

    private String getFileName(Path filePath) {
        return filePath.toAbsolutePath().toString();
    }
}
