package problem.statement;

import problem.statement.impl.CsvProcessorImpl;
import problem.statement.impl.JsonProcessorImpl;
import problem.statement.impl.RandomKeywordServiceImpl;

import java.io.IOException;


public class EtlApplication {
    public static void main(String[] args) throws IOException {
        String directoryName = args[0];
        String outputFileName = args[1];

        if (args.length != 2) {
            System.out.println("Proper Usage is: directory name where input files exist, outputFile");
            System.exit(0);
        }

        KeywordService keywordService = new RandomKeywordServiceImpl();
        InputDataProcessor csvDataProcessor = new CsvProcessorImpl(keywordService);
        InputDataProcessor jsonDataProcessor = new JsonProcessorImpl(keywordService);
        EtlManager etlManager = new EtlManager(csvDataProcessor, jsonDataProcessor);
        etlManager.processInputData(directoryName, outputFileName);
    }

}
