package problem.statement;


import java.io.IOException;

public interface InputDataProcessor {
      /**
       * Process incoming file in context to get collection of data, process it and returns the final output data
       * @param filePath the path of file to be processed
       */
      String processFile(String filePath) throws IOException;
}
