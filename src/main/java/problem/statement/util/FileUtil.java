package problem.statement.util;


import java.io.File;

public class FileUtil {
    public static boolean ensureFileExists(String path) {
        File f = new File(path);
        if(f.exists() && !f.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean ensureDirectoryExists(String path) {
        File f = new File(path);
        if(f.exists() && f.isDirectory()) {
            return true;
        }
        return false;
    }
}
