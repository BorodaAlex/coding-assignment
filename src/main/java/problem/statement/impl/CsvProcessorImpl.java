package problem.statement.impl;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import problem.statement.InputDataProcessorBase;
import problem.statement.KeywordService;
import problem.statement.dto.Site;

import com.google.common.collect.Lists;

public class CsvProcessorImpl extends InputDataProcessorBase {
    private final CSVFormat format;

    public CsvProcessorImpl(KeywordService keywordService) throws IOException {
        super(keywordService);
        // Create the CSVFormat object
        format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
    }

    public String processFile(String filePath) throws IOException {
        // initialize the CSVParser object
        CSVParser parser = new CSVParser(new FileReader(filePath), format);

        List<Site> sites = Lists.newArrayList();
        for (CSVRecord record : parser) {
            Site site = new Site();
            site.setId(record.get("id"));
            site.setName(record.get("name"));
            site.setMobile((byte)(Boolean.parseBoolean(record.get("is mobile")) ? 1 : 0));
            site.setScore(Integer.parseInt(record.get("score")));
            site.setKeywords(keywordService.resolveKeywords(site));
            sites.add(site);
        }
        // close the parser
        parser.close();

        return getResultJson(sites, filePath);
    }
}
