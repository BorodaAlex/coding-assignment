package problem.statement.impl;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import problem.statement.InputDataProcessorBase;
import problem.statement.KeywordService;
import problem.statement.dto.Site;
import problem.statement.util.FileUtil;
import problem.statement.util.Json;

import com.fasterxml.jackson.core.type.TypeReference;

public class JsonProcessorImpl extends InputDataProcessorBase {

    public JsonProcessorImpl(KeywordService keywordService) {
        super(keywordService);
    }

    public String processFile(String filePath) throws IOException {
        if (FileUtil.ensureFileExists(filePath)) {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            String fileContent = new String(encoded, Charset.defaultCharset());

            List<Site> sites = Json.getMapper().readValue(fileContent,
                    new TypeReference<List<Site>>() {
                    });

            sites.forEach(item ->
                    item.setKeywords(keywordService.resolveKeywords(item))
            );

            return getResultJson(sites, filePath);
        } else {
            //todo: log error
        }
        return null;
    }
}
