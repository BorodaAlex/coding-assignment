package problem.statement.impl;

import java.util.List;
import java.util.Random;
import java.util.Set;

import problem.statement.KeywordService;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class RandomKeywordServiceImpl implements KeywordService {

    private final List<String> keywords = Lists.newArrayList("sports", "tennis", "recreation", "travel",
            "japan", "music", "film");

    private Random random = new Random();

    @Override
    public String resolveKeywords(Object site) {
        Set<String> uniqueKeywordsValues = Sets.newHashSet();
        String resultKeywords = getRandomWord();
        uniqueKeywordsValues.add(resultKeywords);
        int counter = 0;
        while (true) {
            String maybeUniqueWord = getRandomWord();
            if (uniqueKeywordsValues.contains(maybeUniqueWord))
                continue;
            else {
                uniqueKeywordsValues.add(maybeUniqueWord);
            }
            resultKeywords = Joiner.on(",").join(resultKeywords, maybeUniqueWord);
            counter++;
            if (counter == 3) {
                break;
            }
        }
        return resultKeywords;
    }

    private String getRandomWord() {
        return keywords.get(random.nextInt(keywords.size()));
    }
}
