package problem.statement;

import java.nio.file.Paths;
import java.util.Collection;

import problem.statement.dto.OutputJson;
import problem.statement.dto.Site;
import problem.statement.util.Json;

import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class InputDataProcessorBase implements InputDataProcessor {
    protected KeywordService keywordService;

    protected InputDataProcessorBase(KeywordService keywordService) {
        this.keywordService = keywordService;
    }

    protected String getResultJson(Collection<Site> sites, String filePath) throws JsonProcessingException {
        OutputJson result = new OutputJson();
        String fileName = Paths.get(filePath).getFileName().toString();
        result.setCollectionId(fileName);
        result.setSites(sites);
        String resultJson = Json.getMapper().writeValueAsString(result);
        return resultJson;
    }
}
